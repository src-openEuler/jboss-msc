%global namedreltag .Final
%global namedversion %{version}%{?namedreltag}
Name:                jboss-msc
Version:             1.2.6
Release:             2
Summary:             JBoss Modular Service Container
License:             LGPLv2+
URL:                 https://github.com/jbossas/jboss-msc
Source0:             https://github.com/jbossas/jboss-msc/archive/%{namedversion}.tar.gz
BuildArch:           noarch
BuildRequires:       maven-local mvn(jdepend:jdepend) mvn(junit:junit) mvn(org.javassist:javassist)
BuildRequires:       mvn(org.jboss:jboss-parent:pom:) mvn(org.jboss:jboss-vfs)
BuildRequires:       mvn(org.jboss.apiviz:apiviz) mvn(org.jboss.byteman:byteman)
BuildRequires:       mvn(org.jboss.byteman:byteman-bmunit) mvn(org.jboss.byteman:byteman-install)
BuildRequires:       mvn(org.jboss.logging:jboss-logging)
BuildRequires:       mvn(org.jboss.logging:jboss-logging-processor)
BuildRequires:       mvn(org.jboss.logmanager:jboss-logmanager)
BuildRequires:       mvn(org.jboss.maven.plugins:maven-injection-plugin)
BuildRequires:       mvn(org.jboss.modules:jboss-modules) mvn(org.jboss.threads:jboss-threads)
BuildRequires:       mvn(org.jboss.spec.javax.annotation:jboss-annotations-api_1.2_spec)
BuildRequires:       java-11-openjdk-devel
Requires:            java-11-openjdk
Requires:            javapackages-tools
%description
This package contains the JBoss Modular Service Container.

%package javadoc
Summary:             Javadoc for %{name}
%description javadoc
This package contains the API documentation for %{name}.

%prep
%setup -q -n jboss-msc-%{namedversion}
%pom_add_dep org.jboss.spec.javax.annotation:jboss-annotations-api_1.2_spec:1.0.2.Final

%build
export JAVA_HOME=%{_jvmdir}/java-11-openjdk
export CFLAGS="${RPM_OPT_FLAGS}"
export CXXFLAGS="${RPM_OPT_FLAGS}"
%mvn_build -f --xmvn-javadoc

%install
%mvn_install

%files -f .mfiles

%files javadoc -f .mfiles-javadoc

%changelog
* Thu Aug 03 2023 Ge Wang <wang__ge@126.com> - 1.2.6-2
- Compile with jdk11 due to jboss-modules updated

* Mon Aug 17 2020 yaokai <yaokai13@huawei.com> - 1.2.6-1
- Package init
